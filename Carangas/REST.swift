//
//  REST.swift
//  Carangas
//
//  Created by Wagner Rodrigues on 08/02/2018.
//  Copyright © 2018 Eric Brito. All rights reserved.
//

import Foundation

enum CarError {
    case url
    case taskError(error: Error)
    case noResponse
    case NoData
    case responseStatusCode(code: Int)
    case invalidJson
}

enum RestOperation {
    case seve
    case update
    case delete
}

class REST {
    
    private static let basePath = "https://carangas.herokuapp.com/cars/"
    
    private static let configuration : URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.httpAdditionalHeaders = ["Content-Type": "application/json"]
        config.timeoutIntervalForRequest = 30.0
        config.httpMaximumConnectionsPerHost = 5
        return config
    }()
    
    private static let session = URLSession(configuration: configuration) //URLSession.shared
    
    class func loadCars(onComplete: @escaping ([Car]) -> Void, onError: @escaping (CarError) -> Void) {
        guard let url = URL(string: basePath) else {
            onError(.url)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error == nil {
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                if response.statusCode == 200 {
                    guard let data = data else { return }
                    do{
                        let cars = try JSONDecoder().decode([Car].self, from: data)
                        onComplete(cars)
                        
                    } catch {
                        print(error.localizedDescription)
                        onError(.invalidJson)
                    }
                } else {
                    print("Algum astatus inválido pelo servidor!")
                    onError(.responseStatusCode(code: response.statusCode))
                }
            } else {
                print(error!)
                onError(.taskError(error: error!))
            }
        }
        dataTask.resume()
    }
    
    class func save(car: Car, onComplete: @escaping (Bool) -> Void) {
        applyOperation(car: car, operation: .seve, onComplete: onComplete)
    }
    
    class func update(car: Car, onComplete: @escaping (Bool) -> Void) {
        applyOperation(car: car, operation: .update, onComplete: onComplete)
       
    }
    
    class func delete(car: Car, onComplete: @escaping (Bool) -> Void) {
        applyOperation(car: car, operation: .delete, onComplete: onComplete)
        
    }
    
    private class func applyOperation(car: Car, operation: RestOperation, onComplete: @escaping (Bool) -> Void) {
        
        
        let urlString = basePath + "/" + (car._id ?? "")
        
        guard let url = URL(string: urlString ) else {
            onComplete(false)
            return
        }
        
        var httpMethod: String = ""
        var request = URLRequest(url: url)
        
        switch operation{
        case .seve:
            httpMethod = "POST"
        case .update:
            httpMethod = "PUT"
        case .delete:
            httpMethod = "DELETE"
        }
        
        request.httpMethod = httpMethod
        guard let json = try? JSONEncoder().encode(car) else {
            onComplete(false)
            return
        }
        
        request.httpBody = json
        
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil {
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 , let _ = data else {
                    onComplete(false)
                    return
                }
                onComplete(true)
                
                
            } else {
                onComplete(false)
            }
        }
        dataTask.resume()
    }
    
    
    class func loadBrands(onComplete: @escaping ([Brand]?) -> Void){
        guard let url = URL(string: "https://fipeapi.appspot.com/api/1/carros/marcas.json") else {
            onComplete(nil)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error == nil {
                guard let response = response as? HTTPURLResponse else {
                     onComplete(nil)
                    return
                }
                if response.statusCode == 200 {
                    guard let data = data else { return }
                    do{
                        let brands = try JSONDecoder().decode([Brand].self, from: data)
                        onComplete(brands)
                        
                    } catch {
                        print(error.localizedDescription)
                         onComplete(nil)
                    }
                } else {
                    print("Algum astatus inválido pelo servidor!")
                    onComplete(nil)
                }
            } else {
                print(error!)
                 onComplete(nil)
            }
        }
        dataTask.resume()
    }
    
    
    
}




















